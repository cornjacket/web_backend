package client

import (
	"bytes"
	"encoding/json"
	"fmt"

	//"errors"
	"net/http"
	//"io/ioutil"
)

type LoginResult struct {
	Status   int           `json:"status"`
	Error    string        `json:"error"`
	Response LoginResponse `json:"response"`
}

type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type LoginResponse struct {
	AvatarPath string `json:"avatar_path"`
	Email      string `json:"email"`
	Id         int64  `json:"id"`
	Token      string `json:"token"`
	Username   string `json:"username"`
}

func (c *WebBackendClient) Login(req LoginRequest) error {

	//req := LoginRequest{Email: "luther@gmail.com", Password: "password"}

	var err error
	url := c.URL() + "login"
	if c.Enabled {
		fmt.Printf("WebBackendClient: loggging into to %s\n", url)
		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(req)
		err = c.SignIn(url, b)
	} else {
		fmt.Printf("WebBackendClient is disabled.\n")
		// TODO(DRT) : Create a custom error here
	}
	return err

}

// This function can be extracted and included in the uService clientlib for a Post function
// post will return error on non 200 or 201 status code
func (c *WebBackendClient) SignIn(url string, b *bytes.Buffer) error {
	var res *http.Response
	var err error
	res, err = http.Post(url, "application/json; charset=utf-8", b) // Should I defer close of the res.Body
	//fmt.Printf("statusCreated: %d\n", http.StatusCreated)
	//fmt.Printf("statusOK: %d\n", http.StatusOK)
	if err == nil {

		if res.StatusCode == http.StatusOK || res.StatusCode == http.StatusCreated {
			// TESTING
			// Stub an empty result to be populated from the body
			r := LoginResult{}

			// Populate the packet data
			json.NewDecoder(res.Body).Decode(&r)
			//fmt.Printf("client.Post response: %v\n", string(r)) GENERATED AN ERROR, can't convert to string
			c.Jwt = r.Response.Token // TODO(DRT) Should I check for existence of Response
		}

		/*		TODO: I need to address failing error codes. i.e. not 200 or 201
				body, _ := ioutil.ReadAll(res.Body)
				fmt.Printf("http_post nil: statusCode: %d\n", res.StatusCode)
				if res.StatusCode != http.StatusOK && res.StatusCode != http.StatusCreated {
					err = errors.New(string(body))
				}
				fmt.Printf("client.Post response: %v\n", string(body))
		*/
	}
	return err
}
