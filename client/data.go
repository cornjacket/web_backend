package client

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	view_client "bitbucket.org/cornjacket/view_hndlr/client"
)

/* copied from view_hndlr/client for convenience...
// DataRecord is a subset of data.Row
type DataRecord struct {
        Ts     int64 `json:"ts"`
        ValueA int   `json:"valuea"`
        ValueB int   `json:"valueb"`
}
type DataStateResp struct {
        TsStart uint64       `json:"start"`
        TsEnd   uint64       `json:"end"`
        Eui     string       `json:"eui"`
        Data    []DataRecord `json:"data"`
}
*/

type DataResponse struct {
	Status   int                       `json:"status"`
	Error    string                    `json:"error"`
	Response view_client.DataStateResp `json:"response"`
}

func (c *WebBackendClient) GetData(eui string, tsStart uint64, tsEnd uint64) (DataResponse, error) {

	var r DataResponse
	if !c.Enabled {
		return r, nil // This should be a custom error indicating the service was not initialized
	}

	if c.Jwt == "" {
		return r, nil // This should be a custom error indicating that the user must login first
	}
	var bearer = "Bearer " + c.Jwt
	fmt.Printf("%s\n", bearer)
	start := strconv.FormatUint(tsStart, 10)
	end := strconv.FormatUint(tsEnd, 10)

	client := &http.Client{}
	url := c.URL() + "data/" + eui + "?start=" + start + "&end=" + end

	req, err := http.NewRequest("GET", url, nil)
	req.Header.Add("Authorization", bearer)
	response, err := client.Do(req)

	if err != nil {
		fmt.Printf("ERROR GET %s: %v\n", url, err)
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)

		if err != nil {
			fmt.Printf("client.Get ERROR\t%s\n", err)
		} else {
			fmt.Printf("client.GetData: %v\n", string(contents))

			err = json.Unmarshal(contents, &r)
			if err != nil {
				fmt.Printf("client.GetData %s ERROR: %s\n", url, err)
			}
		}
	}
	return r, err

}

/* For legacy test purposes....
func (c *WebBackendClient) GetDataTest(eui string, tsStart uint64, tsEnd uint64) (DataResponse, error) {

	var r DataResponse
	if !c.Enabled {
		return r, nil // This should be a custom error indicating the service was not initialized
	}

	start := strconv.FormatUint(tsStart, 10)
	end := strconv.FormatUint(tsEnd, 10)

	client := &http.Client{}
	url := c.URL() + "datatest/" + eui + "?start=" + start + "&end=" + end

	req, err := http.NewRequest("GET", url, nil)
	response, err := client.Do(req)

	if err != nil {
		fmt.Printf("ERROR GET %s: %v\n", url, err)
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)

		if err != nil {
			fmt.Printf("client.Get ERROR\t%s\n", err)
		} else {
			fmt.Printf("client.GetData: %v\n", string(contents))

			err = json.Unmarshal(contents, &r)
			if err != nil {
				fmt.Printf("client.GetData %s ERROR: %s\n", url, err)
			}
		}
	}
	return r, err

}
*/
