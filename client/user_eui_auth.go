package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"bitbucket.org/cornjacket/web_backend/app/models"
)

type AuthRequest struct {
	UserId int64  `json:"user_id"`
	Eui    string `json:"eui"`
}

func (c *WebBackendClient) CreateUserEuiAuth(authReq AuthRequest) error { //eui string, id int64) error {

	// TODO(DRT): Validate request

	if !c.Enabled {
		return nil // This should be a custom error indicating the service was not initialized
	}

	if c.Jwt == "" {
		return nil // This should be a custom error indicating that the user must login first
	}
	var bearer = "Bearer " + c.Jwt
	//fmt.Printf("%s\n", bearer)

	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(authReq)

	client := &http.Client{}
	url := c.URL() + "user_eui_auths"

	req, err := http.NewRequest("POST", url, b)
	req.Header.Add("Authorization", bearer)
	response, err := client.Do(req)

	if err != nil {
		fmt.Printf("ERROR POST CreateAuth %s: %v\n", url, err)
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)

		if err != nil {
			fmt.Printf("client.CreateAuth ERROR\t%s\n", err)
		} else {
			fmt.Printf("client.CreateAuth: %v\n", string(contents))

			//err = json.Unmarshal(contents, &r)
			//if err != nil {
			//	fmt.Printf("client.GetData %s ERROR: %s\n", url, err)
			//}
		}
	}
	return err

}

/* copied here from models.UserEuiAuth for convenience. This could be extracted into a separate file.
type UserEuiAuth struct {
	ID		uint32    `gorm:"primary_key;auto_increment" json:"id"`
	UserID		uint32    `gorm:"not null" json:"user_id"`
	Eui		string    `gorm:"size:255;not null" json:"eui"`
	CreatedAt	time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt	time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}
*/

type UserEuiAuthResponse struct {
	Status   int                `json:"status"`
	Error    string             `json:"error"`
	Response models.UserEuiAuth `json:"response"`
}

// TODO(DRT) - There is a bug in this code

func (c *WebBackendClient) GetUserEuiAuth(id uint64, eui string) (UserEuiAuthResponse, error) {

	resp := UserEuiAuthResponse{}
	// TODO(DRT): Validate request
	if !c.Enabled {
		return resp, nil // This should be a custom error indicating the service was not initialized
	}
	if c.Jwt == "" {
		return resp, nil // This should be a custom error indicating that the user must login first
	}
	if eui == "" {
		return resp, nil // This should be a custom error indicating that the user must login first
	}
	var bearer = "Bearer " + c.Jwt
	//fmt.Printf("%s\n", bearer)

	idString := strconv.FormatUint(id, 10)
	client := &http.Client{}
	url := c.URL() + "user_eui_auths/" + idString + "?eui=" + eui

	req, err := http.NewRequest("GET", url, nil)
	req.Header.Add("Authorization", bearer)
	response, err := client.Do(req)

	if err != nil {
		fmt.Printf("ERROR POST %s: %v\n", url, err)
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)

		if err != nil {
			fmt.Printf("client.GetAuth ERROR\t%s\n", err)
		} else {
			fmt.Printf("client.GetAuth: %v\n", string(contents))

			err = json.Unmarshal(contents, &resp)
			if err != nil {
				fmt.Printf("client.GetAuth %s ERROR: %s\n", url, err)
			}
		}
	}
	return resp, err
}

func (c *WebBackendClient) DeleteUserEuiAuth(id uint64, eui string) (UserEuiAuthResponse, error) {

	resp := UserEuiAuthResponse{}
	// TODO(DRT): Validate request
	if !c.Enabled {
		return resp, nil // This should be a custom error indicating the service was not initialized
	}
	if c.Jwt == "" {
		return resp, nil // This should be a custom error indicating that the user must login first
	}
	if eui == "" {
		return resp, nil // This should be a custom error indicating that the user must login first
	}
	var bearer = "Bearer " + c.Jwt
	//fmt.Printf("%s\n", bearer)

	idString := strconv.FormatUint(id, 10)
	client := &http.Client{}
	url := c.URL() + "user_eui_auths/" + idString + "?eui=" + eui

	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		fmt.Printf("client.DeleteAuth (0) ERROR %s: %v\n", url, err)
	}
	req.Header.Add("Authorization", bearer)
	response, err := client.Do(req)

	if err != nil {
		fmt.Printf("ERROR POST %s: %v\n", url, err)
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)

		if err != nil {
			fmt.Printf("client.DeleteAuth (1) ERROR \t%s\n", err)
		} else {
			fmt.Printf("client.DeleteAuth (2): %v\n", string(contents))
			err = json.Unmarshal(contents, &resp)
			if err != nil {
				fmt.Printf("client.DeleteAuth (3): %s ERROR: %s\n", url, err)
			}
		}
	}
	return resp, err
}
