package client 

type WebBackendClient struct {
	Hostname	string
	Port		string
	Enabled		bool
	Jwt		string
}

// TODO: Add error return value
// TODO: Validate Hostname and Port within reason
func (c *WebBackendClient) Open(Hostname, Port string) error {
	c.Hostname = Hostname
	c.Port = Port
	c.Enabled = true
	return c.Ping()	
}

func (c *WebBackendClient) URL() string {
	if c.Enabled {
		return "http://" + c.Hostname + ":" + c.Port + "/api/v1/"
	}
	return ""
}


// TODO: This needs to be done...
// Used to determine if the dependent service is currently up.
// Ping should make multiple attempts to see if the external service is up before giving up and returning an error.
func (c *WebBackendClient) Ping() error {
        // check if enabled
        // if so, then make a GET call to /ping of the URL()
        // if there is an error, then repeatedly call for X times with linear backoff until success
        /* Keep and add to services...
           // ping loop
           var pingErr error
           for i := 0; i < 15; i++ {
                   fmt.Printf("Pinging %s\n", DbHost)
                   pingErr = server.DB.DB().Ping()
                   if pingErr != nil {
                           fmt.Println(pingErr)
                   } else {
                           fmt.Println("Ping replied.")
                           break;
                   }
                   time.Sleep(time.Duration(3) * time.Second)
           }
           if pingErr != nil {
                   log.Fatal("This is the error:", err)
           }
        */

        return nil
}


