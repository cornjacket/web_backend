package client

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	view_client "bitbucket.org/cornjacket/view_hndlr/client"
)

/* copied from view_hndlr/client for convenience...
type PacketsResp struct {
	TsStart uint64     `json:"start"`
	TsEnd   uint64     `json:"end"`
	Eui     string     `json:"eui"`
	Packets []UpPacket `json:"packets"`
}
*/

type PacketsResponse struct {
	Status   int                     `json:"status"`
	Error    string                  `json:"error"`
	Response view_client.PacketsResp `json:"response"`
}

func (c *WebBackendClient) GetPackets(eui string, tsStart uint64, tsEnd uint64) (PacketsResponse, error) {

	var r PacketsResponse
	if !c.Enabled {
		return r, nil // This should be a custom error indicating the service was not initialized
	}

	if c.Jwt == "" {
		return r, nil // This should be a custom error indicating that the user must login first
	}
	var bearer = "Bearer " + c.Jwt
	fmt.Printf("%s\n", bearer)
	start := strconv.FormatUint(tsStart, 10)
	end := strconv.FormatUint(tsEnd, 10)

	client := &http.Client{}
	url := c.URL() + "packets/" + eui + "?start=" + start + "&end=" + end

	req, err := http.NewRequest("GET", url, nil)
	req.Header.Add("Authorization", bearer)
	response, err := client.Do(req)

	if err != nil {
		fmt.Printf("ERROR GET %s: %v\n", url, err)
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)

		if err != nil {
			fmt.Printf("client.Get ERROR\t%s\n", err)
		} else {
			fmt.Printf("client.GetPackets: %v\n", string(contents))

			err = json.Unmarshal(contents, &r)
			if err != nil {
				fmt.Printf("client.GetPackets %s ERROR: %s\n", url, err)
			}
		}
	}
	return r, err
}
