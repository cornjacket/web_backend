package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"time"

	//"errors"
	"net/http"
	//"io/ioutil"
)

// Copied from app/models/user.go
type User struct {
	ID         uint32    `json:"id"`
	Username   string    `json:"username"`
	Email      string    `json:"email"`
	Password   string    `json:"password"`
	AvatarPath string    `json:"avatar_path"`
	Admin      int       `json:"admin"`
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
}

type UserResult struct {
	Status   int    `json:"status"`
	Error    string `json:"error"`
	Response User   `json:"response"`
}

/*
type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type LoginResponse struct {
	AvatarPath string `json:"avatar_path"`
	Email      string `json:"email"`
	Id         int64  `json:"id"`
	Token      string `json:"token"`
	Username   string `json:"username"`
}
*/

func (c *WebBackendClient) CreateUser(req User) (UserResult, error) {

	// Stub an empty result to be populated from the body
	result := UserResult{}
	var err error
	url := c.URL() + "users"
	if c.Enabled {
		fmt.Printf("WebBackendClient.CreateUser %s\n", url)
		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(req)
		var res *http.Response
		res, err = http.Post(url, "application/json; charset=utf-8", b) // Should I defer close of the res.Body
		if res.StatusCode == http.StatusOK || res.StatusCode == http.StatusCreated {
			// Populate the packet data
			json.NewDecoder(res.Body).Decode(&result)
			fmt.Printf("WebBackendClient.CreateUser statusCode: %d, resultCode: %d\n", res.StatusCode, result.Status)
		}
	} else {
		fmt.Printf("WebBackendClient is disabled.\n")
		// TODO(DRT) : Create a custom error here
	}
	return result, err

}
