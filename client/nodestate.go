package client

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	view_client "bitbucket.org/cornjacket/view_hndlr/client"
)

type NodeStateResponse struct {
	Status   int                   `json:"status"`
	Error    string                `json:"error"`
	Response view_client.NodeState `json:"response"`
}

func (c *WebBackendClient) GetNodeState(eui string) (NodeStateResponse, error) {

	var n NodeStateResponse
	if !c.Enabled {
		return n, nil // This should be a custom error indicating the service was not initialized
	}

	if c.Jwt == "" {
		return n, nil // This should be a custom error indicating that the user must login first
	}
	var bearer = "Bearer " + c.Jwt
	fmt.Printf("%s\n", bearer)

	client := &http.Client{}
	url := c.URL() + "nodes/" + eui

	req, err := http.NewRequest("GET", url, nil)
	req.Header.Add("Authorization", bearer)
	response, err := client.Do(req)

	if err != nil {
		fmt.Printf("ERROR GET %s: %v\n", url, err)
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)

		if err != nil {
			fmt.Printf("client.Get ERROR\t%s\n", err)
		} else {
			fmt.Printf("client.GetNodeState: %v\n", string(contents))

			err = json.Unmarshal(contents, &n)
			if err != nil {
				fmt.Printf("client.GetNodeState %s ERROR: %s\n", url, err)
			}
		}
	}
	return n, err
}
