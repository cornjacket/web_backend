#!/bin/bash

# Postgres Live
export DB_DRIVER=postgres
#Used for creating a JWT. Can be anything 
export API_SECRET=98hbun98h
export DB_USER=postgres
export DB_PASSWORD=docker
export DB_NAME=fullstack_api
export DB_PORT=5432
export WEB_BACKEND_TEST_MODE=true
