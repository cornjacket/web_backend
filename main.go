package main

import (
	"bitbucket.org/cornjacket/web_backend/app"
)

func main() {

	backendApp := app.NewAppService()
	backendApp.Init()
	backendApp.Run()

}
