mod_replace:
	go mod edit -replace bitbucket.org/cornjacket/web_backend/app=/home/david/work/go/src/bitbucket.org/cornjacket/web_backend/app
mod_drop_replace:
	go mod edit -dropreplace bitbucket.org/cornjacket/web_backend/app
docker_local_bash:
	docker exec -it local_web_backend /bin/ash
docker_run_prod:
	# this won't work as I expect because the docker container is not able to talk outside of its container space.
	# i think i need to use a docker-compose flow
	docker run -it --name=local_web_backend --env="DB_PASSWORD=docker" -p 8000:8000 cornjacket/web_backend
docker_run_dev:
	# this won't work as I expect because the docker container is not able to talk outside of its container space.
	# i think i need to use a docker-compose flow
	docker run -it --name=local_web_backend -p 8000:8000 web_backend_dev 
docker_build_prod:
	docker build . -f ./docker/Dockerfile.prod -t cornjacket/web_backend
docker_build_dev:
	docker build . -f ./docker/Dockerfile.dev -t web_backend_dev
postgres_run:
	# THIS DID NOT WORK docker run --rm   --name pg-docker -e POSTGRES_PASSWORD=docker -d -p 5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data  postgres	
postgres_stop:
	docker stop postgres 
postgres_client:
	psql -h localhost -U postgres -d postgres	
