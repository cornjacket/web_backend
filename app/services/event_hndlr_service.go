package services

// DRT - first implemenation will be specific ViewHndlrService, then later it will be a generic service
//     - how will I use specific messages with generic services? That needs to be figured out.

// DRT - later we will have functions like findAllServices(), queryAllServices, ...


import (

	"fmt"
	"io/ioutil"
	"encoding/json"
	"net/http"
	"bitbucket.org/cornjacket/iot/message"
)

type EventHndlrService struct {
	Hostname  string
	Port      string
	Enabled   bool
}

// TODO: Add error return value
// TODO: Validate Hostname and Port within reason
func (s *EventHndlrService) Init(Hostname, Port string) {
	s.Hostname = Hostname
	s.Port = Port
	s.Enabled = true	
}

func (s *EventHndlrService) URL() string {
	if s.Enabled {
		return "http://" + s.Hostname + ":" + s.Port + "/"
	}
	return ""
}

/*
type LastBoot struct {
        Version         int     // if not set, then implies version 0
        LastBoot        int64
}
*/


func (s *EventHndlrService) GetLastBoot() (message.LastBoot, error) {


// TODO: The response to an error should not be to take down the server. simply returning the empty user is sufficient

	//m := message.LastBoot{0, 612000}
	//m := message.LastBoot{Version: 0, LastBoot: int64(12000)}
	var m message.LastBoot
	//m.Version = 0
	//m.LastBoot = int64(12000)
	if !s.Enabled {
		//return message.LastBoot{}, nil // This should be a custom error indicating the service was not initialized
		return m, nil // This should be a custom error indicating the service was not initialized
	}


	client := &http.Client{}
	url := s.URL() + "lora_iot/lastboot" // TODO: This needs to change.

	req, err := http.NewRequest("GET", url, nil)
        response, err := client.Do(req)

	if err != nil {
		fmt.Printf("ERROR GET /lastboot: %v\n", err)
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)

		if err != nil {
			fmt.Printf("ERROR\t%s\n", err)
		}
		fmt.Printf("getUserJson: %v\n", string(contents))

		err = json.Unmarshal(contents, &m)
		if err != nil {
			fmt.Printf("ERROR\t%s\n", err)
		}
	}
	return m, err

}
