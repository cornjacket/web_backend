package models

import (
	"errors"
	"time"

	"github.com/jinzhu/gorm"
)

// TODO: the composide UserID and EUI should be unique.
// TODO: there should be an index on Eui or a composite index on UserId, Eui
type UserEuiAuth struct {
	ID		uint32    `gorm:"primary_key;auto_increment" json:"id"`
	UserID		uint32    `gorm:"not null" json:"user_id"`
	Eui		string    `gorm:"size:255;not null" json:"eui"`
	CreatedAt	time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt	time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (u *UserEuiAuth) Validate() error {
	if u.Eui == "" {
		return errors.New("Required Eui")
	}
	if u.UserID == 0 {
		return errors.New("Invalid Id")
	}
	return nil
}

func (u *UserEuiAuth) Save(db *gorm.DB) (*UserEuiAuth, error) {

	var err error
	err = db.Debug().Create(&u).Error
	if err != nil {
		return &UserEuiAuth{}, err
	}
	return u, nil
}

/*
func (u *User) FindAllUsers(db *gorm.DB) (*[]User, error) {
	var err error
	users := []User{}
	err = db.Debug().Model(&User{}).Limit(100).Find(&users).Error
	if err != nil {
		return &[]User{}, err
	}
	return &users, err
}
*/

func (u *UserEuiAuth) FindUserEuiAuthByIDAndEui(db *gorm.DB, uid uint32, eui string) (*UserEuiAuth, error) {
	var err error
	err = db.Debug().Model(UserEuiAuth{}).Where("user_id = ?", uid).Where("eui = ?", eui).Take(&u).Error
	if err != nil {
		return &UserEuiAuth{}, err
	}
	if gorm.IsRecordNotFoundError(err) {
		return &UserEuiAuth{}, errors.New("UserEuiAuth Not Found")
	}
	return u, err
}

func (u *UserEuiAuth) Delete(db *gorm.DB, uid uint32, eui string) (int64, error) {

	db = db.Debug().Model(&UserEuiAuth{}).Where("user_id = ?", uid).Where("eui = ?", eui).Take(&UserEuiAuth{}).Delete(&UserEuiAuth{})

	if db.Error != nil {
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
