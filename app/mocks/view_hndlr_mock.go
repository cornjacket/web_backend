package mocks

import (
	//"fmt"
	"log"
	//"bitbucket.org/cornjacket/iot/message"
	view_client "bitbucket.org/cornjacket/view_hndlr/client"
)

// Should I rename this clientMock
type ViewHndlrClientMock struct {
}

var ViewLastRxPacketResponse view_client.PacketsResp
var ViewLastDataResponse view_client.DataStateResp
var ViewLastNodeState view_client.NodeState

func SeedDataResponse(dataResponse view_client.DataStateResp) {
	ViewLastDataResponse = dataResponse
}

func SeedPacketsResponse(packetsResponse view_client.PacketsResp) {
	ViewLastRxPacketResponse = packetsResponse
}

func SeedNodeState(nodestate view_client.NodeState) {
	ViewLastNodeState = nodestate
}

// TODO(drt) this is where the package variables should be reset
func (v *ViewHndlrClientMock) Open(Hostname, Port string) error {
	log.Println("ViewHndlrClientMock.Open() invoked.")
	ViewLastRxPacketResponse = view_client.PacketsResp{}
	ViewLastDataResponse = view_client.DataStateResp{}
	ViewLastNodeState = view_client.NodeState{}
	return nil
}

func (v *ViewHndlrClientMock) GetPackets(req view_client.PacketReq) (view_client.PacketsResp, error) {
	log.Println("ViewHndlrClientMock.GetPackets() invoked.")
	return ViewLastRxPacketResponse, nil
}

func (v *ViewHndlrClientMock) GetData(req view_client.DataStateReq) (view_client.DataStateResp, error) {
	log.Println("ViewClientMock.GetData() invoked.")
	return ViewLastDataResponse, nil
}

func (v *ViewHndlrClientMock) GetNodeState(eui string) (view_client.NodeState, error) {
	log.Println("ViewClientMock.GetNodestate() invoked.")
	return ViewLastNodeState, nil
}
