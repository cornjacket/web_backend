package controllers

import (
	"errors"
	"fmt"
	"net/http"

	"bitbucket.org/cornjacket/web_backend/app/auth"
	"bitbucket.org/cornjacket/web_backend/app/models"
	"github.com/gin-gonic/gin"
)

// function should check if user is authorized for the eui in question. should return response code, and error.
// if return nil, then continue
func (app *AppContext) isUserAuthorizedForEui(c *gin.Context) (uid uint32, eui string, statusCode int, err error) {

	eui = c.Param("eui")

	if eui == "" {
		fmt.Printf("auth_helper debug 1. eui: %s\n", eui)
		return 0, eui, http.StatusUnprocessableEntity, errors.New("eui is empty")
	}

	uid, err = auth.ExtractTokenID(c.Request)
	if err != nil {
		fmt.Printf("auth_helper debug 2. eui: %s\n", eui)
		return uid, eui, http.StatusUnauthorized, errors.New("Unauthorized")
	}

	userEuiAuth := models.UserEuiAuth{}
	_, err = userEuiAuth.FindUserEuiAuthByIDAndEui(app.DB, uid, eui)
	if err != nil {

		fmt.Printf("auth_helper debug 3. eui: %s\n", eui)
		// if not approved user, then check if user is an admin
		user := models.User{}
		userGotten, err := user.FindUserByID(app.DB, uint32(uid))
		if err != nil {
			return uid, eui, http.StatusBadRequest, err
		}
		if userGotten.IsAdmin() {
			return uid, eui, http.StatusOK, nil
		}

		return uid, eui, http.StatusUnauthorized, errors.New(http.StatusText(http.StatusUnauthorized))
	}
	fmt.Printf("auth_helper debug 4. eui: %s\n", eui)
	return uid, eui, http.StatusOK, err

}
