package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"bitbucket.org/cornjacket/web_backend/app/auth"
	"bitbucket.org/cornjacket/web_backend/app/models"
	"github.com/gin-gonic/gin"
	//"bitbucket.org/cornjacket/web_backend/app/responses"
	//"bitbucket.org/cornjacket/web_backend/app/utils/formaterror"
)

// Only admins can add and delete UserEuiAuh row. admin=1 inside User record
// First confirm if user is valid
// Then confirm if user is an admin

// STATUS: Extracts user_id and eui from req and creates new UserEuiPair
// TODO:   Still need to confirm that user is an admin (findUser, call IsAdmin)
// TODO:   Validate that Eui is a valid Eui registered with the system centrally
// QUESTION: Should UserEuiAuth mapping be an external service so that this relationship is centralized.
func (app *AppContext) CreateUserEuiAuth(c *gin.Context) {

	//clear previous error if any
	errList = map[string]string{}

	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		fmt.Printf("CreateUserEuiAuth debug 1\n")
		errList["Json format invalid"] = "Not valid json"
		c.JSON(http.StatusUnprocessableEntity, gin.H{
			"status": http.StatusUnprocessableEntity,
			"error":  errList,
		})
		return
	}
	userEuiAuth := models.UserEuiAuth{}
	err = json.Unmarshal(body, &userEuiAuth)
	if err != nil {
		fmt.Printf("CreateUserEuiAuth debug 2\n")
		errList["Marshall error"] = "Not able to unmarshall"
		c.JSON(http.StatusUnprocessableEntity, gin.H{
			"status": http.StatusUnprocessableEntity,
			"error":  errList,
		})
		return
	}
	err = userEuiAuth.Validate()
	if err != nil {
		fmt.Printf("CreateUserEuiAuth debug 2\n")
		errList["Validate error"] = "Eui not valid"
		c.JSON(http.StatusUnprocessableEntity, gin.H{
			"status": http.StatusUnprocessableEntity,
			"error":  errList,
		})
		return
	}
	uid, err := auth.ExtractTokenID(c.Request)
	if err != nil {
		fmt.Printf("CreateUserEuiAuth debug 3\n")
		errList["Token invalid"] = "Not able to extract token"
		c.JSON(http.StatusUnauthorized, gin.H{
			"status": http.StatusUnauthorized,
			"error":  errList,
		})
		return
	}
	user := models.User{}
	userGotten, err := user.FindUserByID(app.DB, uint32(uid))
	if err != nil {
		fmt.Printf("CreateUserEuiAuth debug 4\n")
		errList["User lookup error"] = "User not found"
		c.JSON(http.StatusUnauthorized, gin.H{
			"status": http.StatusUnauthorized,
			"error":  errList,
		})
		return
	}
	if !userGotten.IsAdmin() {
		fmt.Printf("CreateUserEuiAuth debug 5\n")
		errList["User lookup error"] = "Unauthorized"
		c.JSON(http.StatusUnauthorized, gin.H{
			"status": http.StatusUnauthorized,
			"error":  errList,
		})
		return
	}
	userEuiAuthCreated, err := userEuiAuth.Save(app.DB)
	if err != nil {
		fmt.Printf("CreateUserEuiAuth debug 6\n")
		errList["User lookup error"] = "Unauthorized"
		c.JSON(http.StatusInternalServerError, gin.H{
			"status": http.StatusInternalServerError,
			"error":  errList,
		})
		return
	}
	fmt.Printf("CreateUserEuiAuth debug 7\n")
	c.JSON(http.StatusCreated, gin.H{
		"status":   http.StatusCreated,
		"response": userEuiAuthCreated,
	})

}

/* Do I need this? yes, includes a full list of all users and euis. This could get quite large..
func (app *AppContext) GetUserEuiAuths(w http.ResponseWriter, r *http.Request) {

        //clear previous error if any
        errList = map[string]string{}

	user := models.User{}

	users, err := user.FindAllUsers(app.DB)
	if err != nil {
                errList["User lookup error"] = "Unauthorized"
                c.JSON(http.StatusInternalServerError, gin.H{
                        "status": http.StatusInternalServerError,
                        "error":  errList,
                })
		return
	}
	responses.JSON(w, http.StatusOK, users)
}
*/

// admins only
// i would like this function to handle /id?eui=xyz and /id without specifying an eui
func (app *AppContext) GetUserEuiAuth(c *gin.Context) {

	//clear previous error if any
	errList = map[string]string{}

	// confirm calling user is Admin
	adminUid, err := auth.ExtractTokenID(c.Request)
	if err != nil {
		fmt.Printf("GetUserEuiAuth debug 1\n")
		errList["token error"] = "Unauthorized"
		c.JSON(http.StatusUnauthorized, gin.H{
			"status": http.StatusUnauthorized,
			"error":  errList,
		})
		return
	}
	user := models.User{}
	userGotten, err := user.FindUserByID(app.DB, uint32(adminUid))
	if err != nil {
		fmt.Printf("GetUserEuiAuth debug 2\n")
		errList["User lookup error"] = "User not found"
		c.JSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  errList,
		})
		return
	}
	if !userGotten.IsAdmin() {
		fmt.Printf("GetUserEuiAuth debug 3\n")
		errList["User error"] = "User is not admin"
		c.JSON(http.StatusUnauthorized, gin.H{
			"status": http.StatusUnauthorized,
			"error":  errList,
		})
		return
	}

	userID := c.Param("id")
	uid, err := strconv.ParseUint(userID, 10, 32)
	if err != nil {
		fmt.Printf("GetUserEuiAuth debug 4\n")
		errList["User error"] = "User id is not parseable"
		c.JSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  errList,
		})
		return
	}
	eui := c.Request.FormValue("eui")
	if eui == "" {
		fmt.Printf("GetUserEuiAuth debug 5\n")
		errList["eui error"] = "eui not parseable"
		c.JSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  errList,
		})
		return
	}

	userEuiAuth := models.UserEuiAuth{}
	authGotten, err := userEuiAuth.FindUserEuiAuthByIDAndEui(app.DB, uint32(uid), eui)
	if err != nil {
		fmt.Printf("GetUserEuiAuth debug 6\n")
		fmt.Printf("FindUserEuiAuthByIDAndEui error: %s\n", err)
		errList["auth error"] = "user not authorized"
		c.JSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  errList,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status":   http.StatusOK,
		"response": authGotten,
	})
}

// admins only
func (app *AppContext) DeleteUserEuiAuth(c *gin.Context) {

	//clear previous error if any
	errList = map[string]string{}

	// confirm calling user is Admin
	adminUid, err := auth.ExtractTokenID(c.Request)
	if err != nil {
		fmt.Printf("DeleteUserEuiAuth DEBUG 1\n")
		errList["token error"] = "Unauthorized"
		c.JSON(http.StatusUnauthorized, gin.H{
			"status": http.StatusUnauthorized,
			"error":  errList,
		})
		return
	}
	user := models.User{}
	userGotten, err := user.FindUserByID(app.DB, uint32(adminUid))
	if err != nil {
		fmt.Printf("DeleteUserEuiAuth DEBUG 2\n")
		errList["token error"] = "Unauthorized"
		c.JSON(http.StatusUnauthorized, gin.H{
			"status": http.StatusUnauthorized,
			"error":  errList,
		})
		return
	}
	if !userGotten.IsAdmin() {
		fmt.Printf("DeleteUserEuiAuth DEBUG 3\n")
		errList["user error"] = "user not admin"
		c.JSON(http.StatusUnauthorized, gin.H{
			"status": http.StatusUnauthorized,
			"error":  errList,
		})
		return
	}

	userID := c.Param("id")
	uid, err := strconv.ParseUint(userID, 10, 32)
	if err != nil {
		fmt.Printf("DeleteUserEuiAuth DEBUG 4\n")
		errList["User error"] = "User id is not parseable"
		c.JSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  errList,
		})
		return
	}

	eui := c.Request.FormValue("eui")
	if eui == "" {
		fmt.Printf("DeleteUserEuiAuth DEBUG 5\n")
		// TODO(DRT): Later if the eui is not provided, then we will delete all Eui's for the user id
		errList["eui error"] = "eui not provided"
		c.JSON(http.StatusUnauthorized, gin.H{
			"status": http.StatusUnauthorized,
			"error":  errList,
		})
		return
	}
	fmt.Printf("DeleteUserEuiAuth DEBUG eui: %s, uid: %d\n", eui, uid)
	userEuiAuth := models.UserEuiAuth{}
	_, err = userEuiAuth.Delete(app.DB, uint32(uid), eui)
	if err != nil {
		fmt.Printf("DEBUG 6\n")
		errList["server error"] = "database error"
		c.JSON(http.StatusInternalServerError, gin.H{
			"status": http.StatusInternalServerError,
			"error":  errList,
		})
		return
	}
	fmt.Printf("DEBUG 7\n")
	c.JSON(http.StatusOK, gin.H{
		"status":   http.StatusOK,
		"response": userEuiAuth,
	})
}
