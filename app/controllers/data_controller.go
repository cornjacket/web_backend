package controllers

import (
	"fmt"
	"net/http"
	"strconv"

	view_client "bitbucket.org/cornjacket/view_hndlr/client"
	"github.com/gin-gonic/gin"
)

func (app *AppContext) GetDataFromViewService(c *gin.Context) {

	_, eui, statusCode, err := app.isUserAuthorizedForEui(c)
	if err != nil {
		fmt.Printf("GetData debug 1\n")
		//errList["Not_Authorized"] = "No Auth Found" // TODO(drt) should this be err.String()
		c.JSON(statusCode, gin.H{
			"status": statusCode,
			"error":  "No Auth Found",
		})
		return
	}

	req := view_client.DataStateReq{}
	req.Eui = eui
	req.TsStart, err = strconv.ParseUint(c.Request.FormValue("start"), 10, 32)
	if err != nil {
		fmt.Printf("GetData debug 2\n")
		//errList["No_start"] = "No/Bad Start Value"
		c.JSON(http.StatusNotFound, gin.H{
			"status": http.StatusNotFound,
			"error":  "No/Bad Start Value",
		})
		return
	}
	req.TsEnd, err = strconv.ParseUint(c.Request.FormValue("end"), 10, 32)
	if err != nil {
		fmt.Printf("GetData debug 3\n")
		//errList["No_end"] = "No/Bad End Value"
		c.JSON(http.StatusNotFound, gin.H{
			"status": http.StatusNotFound,
			"error":  "No/Bad End Value",
		})
		return
	}
	err = req.Validate()
	if err != nil {
		fmt.Printf("GetData debug 4\n")
		//errList["No_validate"] = "Req Validation Failed"
		c.JSON(http.StatusNotFound, gin.H{
			"status": http.StatusNotFound,
			"error":  "Req Validation Failed",
		})
		return
	}

	data, err := app.ViewHndlrService.GetData(req)
	if err != nil {
		fmt.Printf("GetData debug 5\n")
		//errList["No_data"] = "No Data Found"
		c.JSON(http.StatusNotFound, gin.H{
			"status": http.StatusNotFound,
			"error":  "No Data Found",
		})
		return
	}
	fmt.Printf("GetData debug 6\n")
	c.JSON(http.StatusOK, gin.H{
		"status":   http.StatusOK,
		"response": data,
	})

}

func (app *AppContext) GetDataFromViewServiceTest(c *gin.Context) {

	req := view_client.DataStateReq{}

	eui := c.Param("eui")

	if eui == "" {
		fmt.Printf("auth_helper debug 1. eui: %s\n", eui)
		//errList["No_start"] = "Eui is empty."
		c.JSON(http.StatusNotFound, gin.H{
			"status": http.StatusNotFound,
			"error":  "Eui is empty.",
		})
		return
	}

	var err error
	req.Eui = eui
	req.TsStart, err = strconv.ParseUint(c.Request.FormValue("start"), 10, 32)
	if err != nil {
		fmt.Printf("GetData debug 2\n")
		//errList["No_start"] = "No/Bad Start Value"
		c.JSON(http.StatusNotFound, gin.H{
			"status": http.StatusNotFound,
			"error":  "No/Bad Start Value",
		})
		return
	}
	req.TsEnd, err = strconv.ParseUint(c.Request.FormValue("end"), 10, 32)
	if err != nil {
		fmt.Printf("GetData debug 3\n")
		//errList["No_end"] = "No/Bad End Value"
		c.JSON(http.StatusNotFound, gin.H{
			"status": http.StatusNotFound,
			"error":  "No/Bad End Value",
		})
		return
	}
	err = req.Validate()
	if err != nil {
		fmt.Printf("GetData debug 4\n")
		//errList["No_validate"] = "Req Validation Failed"
		c.JSON(http.StatusNotFound, gin.H{
			"status": http.StatusNotFound,
			"error":  "Req Validation Failed",
		})
		return
	}

	data, err := app.ViewHndlrService.GetData(req)
	if err != nil {
		fmt.Printf("GetData debug 5\n")
		//errList["No_data"] = "No Data Found"
		c.JSON(http.StatusNotFound, gin.H{
			"status": http.StatusNotFound,
			"error":  "No Data Found",
		})
		return
	}
	fmt.Printf("GetData debug 6\n")
	c.JSON(http.StatusOK, gin.H{
		"status":   http.StatusOK,
		"response": data,
	})

}
