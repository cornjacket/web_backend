package controllers

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func (app *AppContext) GetNodeStateFromViewService(c *gin.Context) {

	//clear previous error if any
	errList = map[string]string{}

	_, eui, statusCode, err := app.isUserAuthorizedForEui(c)
	if err != nil {
		fmt.Printf("GetData debug 1\n")
		//errList["Not_Authorized"] = "No Auth Found"
		c.JSON(statusCode, gin.H{
			"status": statusCode,
			"error":  "No Auth Found", //errList,
		})
		return
	}

	fmt.Printf("GetNodeStateFromViewService eui: %s\n", eui)
	nodestate, _ := app.ViewHndlrService.GetNodeState(eui) // TODO(drt): Need to add if/else on error?
	c.JSON(http.StatusOK, gin.H{
		"status":   http.StatusOK,
		"response": nodestate,
	})

}
