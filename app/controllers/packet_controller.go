package controllers

import (
	"fmt"
	"net/http"
	"strconv"

	view_client "bitbucket.org/cornjacket/view_hndlr/client"
	"github.com/gin-gonic/gin"
)

func (app *AppContext) GetPacketsFromViewService(c *gin.Context) {

	//clear previous error if any
	errList = map[string]string{}

	req := view_client.PacketReq{}

	_, eui, statusCode, err := app.isUserAuthorizedForEui(c)
	if err != nil {
		errList["Not_Authorized"] = "No Auth Found"
		c.JSON(statusCode, gin.H{
			"status": statusCode,
			"error":  errList,
		})
		return
	}

	req.Eui = eui
	req.TsStart, err = strconv.ParseUint(c.Request.FormValue("start"), 10, 32)
	if err != nil {
		errList["Bad Input"] = "Start value not provided"
		c.JSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  errList,
		})
		return
	}
	req.TsEnd, err = strconv.ParseUint(c.Request.FormValue("end"), 10, 32)
	if err != nil {
		errList["Bad Input"] = "End value not provided"
		c.JSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  errList,
		})
		return
	}
	err = req.Validate()
	if err != nil {
		errList["Bad Input"] = "Eui not provided"
		c.JSON(http.StatusUnprocessableEntity, gin.H{
			"status": http.StatusUnprocessableEntity,
			"error":  errList,
		})
		return
	}
	fmt.Printf("GetPacketsFromViewService eui: %s, start: %d, end: %d\n", req.Eui, req.TsStart, req.TsEnd)

	packets, _ := app.ViewHndlrService.GetPackets(req) // TODO(drt): Need to add if/else on error?
	c.JSON(http.StatusOK, gin.H{
		"status":   http.StatusOK,
		"response": packets,
	})

}
