package controllers

import (
	"net/http"

	"bitbucket.org/cornjacket/web_backend/app/responses"
)

func (app *AppContext) Home(w http.ResponseWriter, r *http.Request) {
	responses.JSON(w, http.StatusOK, "EndPoint Up")

}
