package controllers

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"

	view_client "bitbucket.org/cornjacket/view_hndlr/client"
	"bitbucket.org/cornjacket/web_backend/app/middlewares"
	"bitbucket.org/cornjacket/web_backend/app/services"
	_ "github.com/jinzhu/gorm/dialects/mysql"    //mysql database driver
	_ "github.com/jinzhu/gorm/dialects/postgres" //postgres database driver
)

type ViewHandlerGetInterface interface {
	Open(Hostname, Port string) error
	GetData(req view_client.DataStateReq) (view_client.DataStateResp, error)
	GetNodeState(eui string) (view_client.NodeState, error)
	GetPackets(req view_client.PacketReq) (view_client.PacketsResp, error)
}

type AppContext struct {
	DB                *gorm.DB
	Router            *gin.Engine
	EventHndlrService services.EventHndlrService
	//ViewHndlrService  view_client.ViewHndlrService
	ViewHndlrService ViewHandlerGetInterface
}

func NewAppContext() *AppContext {
	context := AppContext{}
	context.ViewHndlrService = &view_client.ViewHndlrService{} // implements ViewHandlrGetInterface
	return &context
}

var errList = make(map[string]string)

func (app *AppContext) Run(addr string) {

	app.Router = gin.Default()
	app.Router.Use(middlewares.CORSMiddleware())
	app.initializeRoutes()

	fmt.Printf("Listening to port %s\n", addr)
	log.Fatal(http.ListenAndServe(":"+addr, app.Router))
}
