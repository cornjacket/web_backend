package controllers

import (

	"net/http"
	"bitbucket.org/cornjacket/web_backend/app/responses"
	//"bitbucket.org/cornjacket/web_backend/api/utils/formaterror"
)

func (app *AppContext) GetEventHndlrServiceStatus(w http.ResponseWriter, r *http.Request) {

	// TODO: There needs to be an error result returned with this function...
	status, _ := app.EventHndlrService.GetLastBoot()
	responses.JSON(w, http.StatusOK, status)

}
