package controllers

import "bitbucket.org/cornjacket/web_backend/app/middlewares"

func (app *AppContext) initializeRoutes() {

	v1 := app.Router.Group("/api/v1")
	{
		// Login Route
		v1.POST("/login", app.Login)

		// Reset password:
		v1.POST("/password/forgot", app.ForgotPassword)
		v1.POST("/password/reset", app.ResetPassword)

		// Users routes
		v1.POST("/users", app.CreateUser) // TODO(DRT) - only admin can execute or if no users
		v1.GET("/users", app.GetUsers)    // TODO(DRT) - only admin can execute
		v1.GET("/users/:id", app.GetUser) // TODO(DRT) - only admin or specific user
		v1.PUT("/users/:id", middlewares.TokenAuthMiddleware(), app.UpdateUser)
		v1.PUT("/avatar/users/:id", middlewares.TokenAuthMiddleware(), app.UpdateAvatar)
		v1.DELETE("/users/:id", middlewares.TokenAuthMiddleware(), app.DeleteUser)

		// Posts routes
		v1.POST("/posts", middlewares.TokenAuthMiddleware(), app.CreatePost)
		v1.GET("/posts", app.GetPosts)
		v1.GET("/posts/:id", app.GetPost)
		v1.PUT("/posts/:id", middlewares.TokenAuthMiddleware(), app.UpdatePost)
		v1.DELETE("/posts/:id", middlewares.TokenAuthMiddleware(), app.DeletePost)
		v1.GET("/user_posts/:id", app.GetUserPosts)

		// Data route - TODO(drt) - need to change from data to app_states or similar
		v1.GET("/data/:eui", middlewares.TokenAuthMiddleware(), app.GetDataFromViewService)
		// Data route - TODO(drt) - need to change from data to app_states or simila
		//v1.GET("/datatest/:eui", app.GetDataFromViewServiceTest) // testing purposes - no auth

		// UserEuiAuth routes
		v1.POST("/user_eui_auths", middlewares.TokenAuthMiddleware(), app.CreateUserEuiAuth)
		v1.GET("/user_eui_auths/:id", middlewares.TokenAuthMiddleware(), app.GetUserEuiAuth)
		v1.DELETE("/user_eui_auths/:id", middlewares.TokenAuthMiddleware(), app.DeleteUserEuiAuth)

		// EUI route
		v1.GET("/nodes/:eui", middlewares.TokenAuthMiddleware(), app.GetNodeStateFromViewService)

		// Packet route
		v1.GET("/packets/:eui", middlewares.TokenAuthMiddleware(), app.GetPacketsFromViewService)

	}
}
