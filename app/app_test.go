package app

import (
	"fmt"
	"log"
	"net/http"
	"testing"
	"time"

	. "gopkg.in/check.v1"

	//"bitbucket.org/cornjacket/iot/message"

	view_client "bitbucket.org/cornjacket/view_hndlr/client"
	"bitbucket.org/cornjacket/web_backend/app/controllers"
	"bitbucket.org/cornjacket/web_backend/app/mocks"
	backend_client "bitbucket.org/cornjacket/web_backend/client"
)

var user backend_client.WebBackendClient
var admin backend_client.WebBackendClient
var test backend_client.WebBackendClient

func NewAppServiceMock() AppService {
	a := AppService{}
	a.Context = NewAppContextMock()
	return a
}

func NewAppContextMock() *controllers.AppContext {
	context := controllers.AppContext{}
	context.ViewHndlrService = &mocks.ViewHndlrClientMock{} // implements ViewHandlrGetInterface
	return &context
}

// Hook up gocheck into the "go test" runner.
func Test(t *testing.T) { TestingT(t) }

type TestSuite struct {
	AppService AppService
}

var _ = Suite(&TestSuite{})

func (s *TestSuite) SetUpSuite(c *C) {
	fmt.Println("SetUpSuite() invoked")
	s.AppService = NewAppServiceMock()
	//s.AppService = NewAppService()
	s.AppService.InitEnv()
	s.AppService.SetupDatabase()
	s.AppService.DropTables()
	s.AppService.CreateTables()
	go s.AppService.Run()
	time.Sleep(1 * time.Second) // time delay to let server boot and talk to database
	if err := user.Open("localhost", s.AppService.Env.PortNum); err != nil {
		//log.Fatal("client.Open() error: %s", err)
		log.Fatal("user.Open() failed")
	}
	if err := admin.Open("localhost", s.AppService.Env.PortNum); err != nil {
		//log.Fatal("client.Open() error: %s", err)
		log.Fatal("admin.Open() failed")
	}
	if err := test.Open("localhost", s.AppService.Env.PortNum); err != nil {
		//log.Fatal("client.Open() error: %s", err)
		log.Fatal("test.Open() failed")
	}
}

func (s *TestSuite) SetUpTest(c *C) {
	fmt.Println("SetupTest() invoked")
	s.AppService.CreateTables()

	if err := s.AppService.Context.ViewHndlrService.Open("localhost", "8082"); err != nil {
		log.Fatal("ViewHndlrService.Open() error: ", err)
	}

	userReq := backend_client.User{
		Username:   "admin",
		Email:      "admin@test.com",
		Password:   "password",
		AvatarPath: "",
		Admin:      1,
	}

	userResult, err := admin.CreateUser(userReq)
	c.Assert(err, Equals, nil)
	c.Assert(userResult.Status, Equals, http.StatusCreated)
	c.Assert(userResult.Response.ID, Equals, uint32(1))
	c.Assert(userResult.Response.Admin, Equals, 1)

	userReq = backend_client.User{
		Username:   "user",
		Email:      "user@test.com",
		Password:   "password",
		AvatarPath: "",
		Admin:      0,
	}

	userResult, err = user.CreateUser(userReq)
	c.Assert(err, Equals, nil)
	c.Assert(userResult.Status, Equals, http.StatusCreated)
	c.Assert(userResult.Response.ID, Equals, uint32(2))
	c.Assert(userResult.Response.Admin, Equals, 0)

	admin.Login(backend_client.LoginRequest{Email: "admin@test.com", Password: "password"})
	// should this return a user_id

	user.Login(backend_client.LoginRequest{Email: "user@test.com", Password: "password"})
	// should this return a user id

}

func (s *TestSuite) TearDownTest(c *C) {
	fmt.Println("TearDownTest() invoked")
	s.AppService.DropTables()

}

func (s *TestSuite) TearDownSuite(c *C) {
	fmt.Println("TearDownSuite() invoked")
	// TODO(drt) - close the db connection
}

/*
// DataRecord is a subset of data.Row
type DataRecord struct {
        Ts     int64 `json:"ts"`
        ValueA int   `json:"valuea"`
        ValueB int   `json:"valueb"`
}

type DataStateResp struct {
        TsStart uint64       `json:"start"`
        TsEnd   uint64       `json:"end"`
        Eui     string       `json:"eui"`
        Data    []DataRecord `json:"data"`
}
*/

func (s *TestSuite) TestGetData(c *C) {
	fmt.Printf("GetDataTest\n")

	eui := "000000000000123a"

	// TODO(DRT) - need to make the userId a variable that is recvd from backend
	admin.CreateUserEuiAuth(backend_client.AuthRequest{Eui: eui, UserId: 2}) //should check for success
	authResult, err := admin.GetUserEuiAuth(2, eui)
	c.Assert(err, Equals, nil)
	c.Assert(authResult.Status, Equals, http.StatusOK)
	c.Assert(authResult.Response.Eui, Equals, eui)

	tsStart := uint64(10)
	tsEnd := uint64(100)

	// Seed the mock
	var dataRecordList []view_client.DataRecord
	dataRecordList = append(dataRecordList, view_client.DataRecord{Ts: int64(100), ValueA: 1, ValueB: 1})
	dataResponse := view_client.DataStateResp{Eui: eui, TsStart: tsStart, TsEnd: tsEnd, Data: dataRecordList}
	mocks.SeedDataResponse(dataResponse)

	dataResult, err := user.GetData(eui, tsStart, tsEnd)
	c.Assert(err, Equals, nil)
	c.Assert(dataResult.Status, Equals, http.StatusOK)
	c.Assert(dataResult.Response.Eui, Equals, eui)
	c.Assert(len(dataResult.Response.Data), Equals, 1)

	time.Sleep(100 * time.Millisecond) // This is needed so that the TestTearDown func doesn't drop the tables before the event_hndlr is finished processing
}

func (s *TestSuite) TestGetPackets(c *C) {
	fmt.Printf("GetPacketsTest\n")

	eui := "000000000000123a"

	// TODO(DRT) - need to make the userId a variable that is recvd from backend
	admin.CreateUserEuiAuth(backend_client.AuthRequest{Eui: eui, UserId: 2}) //should check for success
	authResult, err := admin.GetUserEuiAuth(2, eui)
	c.Assert(err, Equals, nil)
	c.Assert(authResult.Status, Equals, http.StatusOK)
	c.Assert(authResult.Response.Eui, Equals, eui)

	tsStart := uint64(10)
	tsEnd := uint64(100)

	// Seed the mock

	// TODO(DRT) - this UpPacket is defined in view_hndlr/app/controllers. It should be removed from here and defined
	// inside of iot lib

	var packetList []view_client.UpPacket
	// There are more fields to add into the packet...
	packetList = append(packetList, view_client.UpPacket{Ts: uint64(100), Data: "112233445566778899aabb"})
	packetsResponse := view_client.PacketsResp{Eui: eui, TsStart: tsStart, TsEnd: tsEnd, Packets: packetList}
	mocks.SeedPacketsResponse(packetsResponse)

	packetsResult, err := user.GetPackets(eui, tsStart, tsEnd)
	c.Assert(err, Equals, nil)
	c.Assert(packetsResult.Status, Equals, http.StatusOK)
	c.Assert(packetsResult.Response.Eui, Equals, eui)
	c.Assert(len(packetsResult.Response.Packets), Equals, 1)

	time.Sleep(100 * time.Millisecond) // This is needed so that the TestTearDown func doesn't drop the tables before the event_hndlr is finished processing
}

func (s *TestSuite) TestGetNodeState(c *C) {
	fmt.Printf("GetNodeStateTest\n")

	eui := "000000000000123a"

	// TODO(DRT) - need to make the userId a variable that is recvd from backend
	admin.CreateUserEuiAuth(backend_client.AuthRequest{Eui: eui, UserId: 2}) //should check for success
	authResult, err := admin.GetUserEuiAuth(2, eui)
	c.Assert(err, Equals, nil)
	c.Assert(authResult.Status, Equals, http.StatusOK)
	c.Assert(authResult.Response.Eui, Equals, eui)

	// Seed the mock
	mocks.SeedNodeState(view_client.NodeState{Eui: eui, AppNum: 4})

	nodeStateResult, err := user.GetNodeState(eui)
	c.Assert(err, Equals, nil)
	c.Assert(nodeStateResult.Status, Equals, http.StatusOK)
	c.Assert(nodeStateResult.Response.Eui, Equals, eui)
	c.Assert(nodeStateResult.Response.AppNum, Equals, 4)

	time.Sleep(100 * time.Millisecond) // This is needed so that the TestTearDown func doesn't drop the tables before the event_hndlr is finished processing
}

func (s *TestSuite) TestCreateUser(c *C) {
	fmt.Printf("CreateUserTest\n")

	userReq := backend_client.User{
		Username:   "david",
		Email:      "david@test.com",
		Password:   "123456",
		AvatarPath: "",
		Admin:      1,
	}

	userResult, err := test.CreateUser(userReq)
	c.Assert(err, Equals, nil)
	c.Assert(userResult.Status, Equals, http.StatusCreated)
	c.Assert(userResult.Response.ID, Equals, uint32(3))
	c.Assert(userResult.Response.Admin, Equals, 1)

	time.Sleep(100 * time.Millisecond) // This is needed so that the TestTearDown func doesn't drop the tables before the event_hndlr is finished processing
}

func (s *TestSuite) TestCreateUserEuiAuth(c *C) {
	fmt.Printf("CreateUserEuiAuthTest\n")

	eui := "000000000000123a"

	// TODO(DRT) - need to make the userId a variable that is recvd from backend
	admin.CreateUserEuiAuth(backend_client.AuthRequest{Eui: eui, UserId: 2}) //should check for success
	authResult, err := admin.GetUserEuiAuth(2, eui)
	c.Assert(err, Equals, nil)
	c.Assert(authResult.Status, Equals, http.StatusOK)
	c.Assert(authResult.Response.Eui, Equals, eui)

	time.Sleep(100 * time.Millisecond) // This is needed so that the TestTearDown func doesn't drop the tables before the event_hndlr is finished processing
}

func (s *TestSuite) TestDeleteUserEuiAuth(c *C) {
	fmt.Printf("CreateUserEuiAuthTest\n")

	eui := "000000000000123a"

	// TODO(DRT) - need to make the userId a variable that is recvd from backend
	admin.CreateUserEuiAuth(backend_client.AuthRequest{Eui: eui, UserId: 2}) //should check for success
	authResult, err := admin.GetUserEuiAuth(2, eui)
	c.Assert(err, Equals, nil)
	c.Assert(authResult.Status, Equals, http.StatusOK)
	c.Assert(authResult.Response.Eui, Equals, eui)

	// now let's delete the auth TODO(DRT) - why is this faiing?

	authResult, err = admin.DeleteUserEuiAuth(2, eui)
	c.Assert(err, Equals, nil)
	c.Assert(authResult.Status, Equals, http.StatusOK)

	time.Sleep(100 * time.Millisecond) // This is needed so that the TestTearDown func doesn't drop the tables before the event_hndlr is finished processing
}
