package app

import (
	"fmt"
	"log"

	"bitbucket.org/cornjacket/web_backend/app/controllers"
	"bitbucket.org/cornjacket/web_backend/app/models"
	"bitbucket.org/cornjacket/web_backend/app/utils/env"
	"github.com/jinzhu/gorm"
)

type AppService struct {
	Env     AppEnv
	Context *controllers.AppContext
}

type AppEnv struct {
	DbPassword string
	DbHostname string
	DbUsername string
	DbPort     string
	DbName     string
	DbDriver   string
	PortNum    string
	TestMode   string
}

func NewAppService() AppService {
	a := AppService{}
	a.Context = controllers.NewAppContext()
	return a
}

func (a *AppService) InitEnv() {

	a.Env.DbPassword = env.GetVar("DB_PASSWORD", "", true) // dbPassword is required for app to not exit
	a.Env.DbHostname = env.GetVar("DB_HOSTNAME", "localhost", false)
	a.Env.DbUsername = env.GetVar("DB_USERNAME", "postgres", false)
	a.Env.DbPort = env.GetVar("DB_PORT", "5432", false)
	a.Env.DbName = env.GetVar("DB_NAME", "fullstack_api", false)
	a.Env.DbDriver = env.GetVar("DB_DRIVER", "postgres", false) // assume postgres flow on localhost by default
	a.Env.PortNum = env.GetVar("WEB_BACKEND_PORT_NUM", "8000", false)
	a.Env.TestMode = env.GetVar("WEB_BACKEND_TEST_MODE", "false", false) // if testmode then enable db seeding

}

func (a *AppService) SetupExternalServices() {

	viewHndlrHost := env.GetVar("VIEW_HNDLR_HOST", "localhost", false)
	viewHndlrPort := env.GetVar("VIEW_HNDLR_PORT", "8082", false)

	// not sure if I am goin to use discovery with the CQRS pattern...
	//discoveryHostname := env.GetVar("DISCOVERY_HOSTNAME", "", false)
	//discoveryPort := env.GetVar("DISCOVERY_PORT", "8001", false)

	if a.Context == nil {
		log.Fatal("AppService.Context == nil. Quitting\n")
	}

	var err error
	if err = a.Context.ViewHndlrService.Open(viewHndlrHost, viewHndlrPort); err != nil {
		log.Fatal("ViewHndrlService.Open() error: ", err)
	}

}

func (a *AppService) Init() {
	a.InitEnv()
	a.SetupDatabase()
	a.DropTables()
	a.CreateTables()
	a.SetupExternalServices()
}

func (a *AppService) Run() {

	if a.Context == nil {
		log.Fatal("AppService.Context == nil. Quitting\n")
	}
	//if a.Env.TestMode != "false" {
	//	seed.Load(a.Context.DB) // This is used for manual testing for faster login - have somewhere else for component testing
	//}

	a.Context.Run(a.Env.PortNum)

}

// TODO(DRT): This needs to be remade
// Following function is for test purposes only
func (a *AppService) DropTables() {

	err := a.Context.DB.Debug().DropTableIfExists(&models.Post{}, &models.User{},
		&models.UserEuiAuth{}, &models.ResetPassword{}).Error
	if err != nil {
		log.Fatalf("cannot drop table: %v", err)
	}
	fmt.Printf("-------------------------- Tables dropped invoked.\n")
}

// TODO(DRT) - this needs to be remade
func (a *AppService) SetupDatabase() {
	if a.Context == nil {
		log.Fatal("SetupDatabase(): AppService.Context == nil. Quitting\n")
	}

	var err error
	// DRT - This code was causing the system to stall here. Removed just to get remainder of flow working.
	// I will need to be able to auto create the database if not exists for CI flow.
	/*
		// Is this needed any more? Is it the right way to do it?
		err = database.CreateDatabaseIfNotExists(a.Env.DbName, a.Env.DbUsername, a.Env.DbPassword, a.Env.DbHostname, a.Env.DbPort)
		if err != nil {
			fmt.Printf("init\tcreateDatabase() failed: %s\n", a.Env.DbName)
			panic(err.Error())
		}
	*/

	if a.Env.DbDriver == "mysql" {
		DBURL := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
			a.Env.DbUsername, a.Env.DbPassword, a.Env.DbHostname, a.Env.DbPort, a.Env.DbName)
		a.Context.DB, err = gorm.Open(a.Env.DbDriver, DBURL)
		if err != nil {
			fmt.Printf("Cannot connect to %s database\n", a.Env.DbDriver)
			log.Fatal("This is the error:", err)
		} else {
			fmt.Printf("We are connected to the %s database\n", a.Env.DbDriver)
		}
	}
	if a.Env.DbDriver == "postgres" {
		DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s",
			a.Env.DbHostname, a.Env.DbPort, a.Env.DbUsername, a.Env.DbName, a.Env.DbPassword)
		a.Context.DB, err = gorm.Open(a.Env.DbDriver, DBURL)
		if err != nil {
			fmt.Printf("Cannot connect to %s database\n", a.Env.DbDriver)
			log.Fatal("This is the error:", err)
		} else {
			fmt.Printf("We are connected to the %s database\n", a.Env.DbDriver)
		}

	}
	if a.Env.DbDriver == "sqlite3" {
		//DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", DbHost, DbPort, DbUser, DbName, DbPassword)
		a.Context.DB, err = gorm.Open(a.Env.DbDriver, a.Env.DbName)
		if err != nil {
			fmt.Printf("Cannot connect to %s database\n", a.Env.DbDriver)
			log.Fatal("This is the error:", err)
		} else {
			fmt.Printf("We are connected to the %s database\n", a.Env.DbDriver)
		}
		a.Context.DB.Exec("PRAGMA foreign_keys = ON")
	}

	// Gorm may handle the ping and so this may not be needed.
	// ping loop
	/*	Let's remove this for now.
		var pingErr error
		for i := 0; i < 15; i++ {
			fmt.Printf("Pinging %s\n", a.Env.DbHostname)
			pingErr = a.Context.DB.DB().Ping()
			if pingErr != nil {
				fmt.Println(pingErr)
			} else {
				fmt.Println("Ping replied.")
				break
			}
			time.Sleep(time.Duration(3) * time.Second)
		}
		if pingErr != nil {
			log.Fatal("This is the error:", err)
		}
	*/
}

func (a *AppService) CreateTables() {
	fmt.Printf("-------------------------- Tables created invoked.\n")

	//database migration
	a.Context.DB.Debug().AutoMigrate(
		&models.User{},
		&models.Post{},
		&models.UserEuiAuth{},
		&models.ResetPassword{},
	)
}
