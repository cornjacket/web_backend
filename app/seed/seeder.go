package seed

import (
	"log"

	"bitbucket.org/cornjacket/web_backend/app/models"
	"github.com/jinzhu/gorm"
)

var users = []models.User{
	models.User{
		Username: "Steven victor",
		Email:    "steven@gmail.com",
		Password: "password",
		Admin:    1,
	},
	models.User{
		Username: "Martin Luther",
		Email:    "luther@gmail.com",
		Password: "password",
		Admin:    0,
	},
}

var posts = []models.Post{
	models.Post{
		Title:   "Title 1",
		Content: "Hello world 1",
	},
	models.Post{
		Title:   "Title 2",
		Content: "Hello world 2",
	},
}

// DRT - Note that there needs to be some way to cross reference the UserId to the Id of the Users table. The id must be a valid number...
var userEuiAuths = []models.UserEuiAuth{
	models.UserEuiAuth{
		UserID: 1,
		Eui:    "000000000000123b",
	},
}

func Load(db *gorm.DB) {

	err := db.Debug().DropTableIfExists(&models.Post{}, &models.User{},
		&models.UserEuiAuth{}, &models.ResetPassword{}).Error
	if err != nil {
		log.Fatalf("cannot drop table: %v", err)
	}
	err = db.Debug().AutoMigrate(&models.User{}, &models.Post{},
		&models.UserEuiAuth{}, &models.ResetPassword{}).Error
	if err != nil {
		log.Fatalf("cannot migrate table: %v", err)
	}

	/*
		err = db.Debug().Model(&models.Post{}).AddForeignKey("author_id", "users(id)", "cascade", "cascade").Error
		if err != nil {
			log.Fatalf("attaching foreign key error: %v", err)
		}
	*/

	for i, _ := range users {
		err = db.Debug().Model(&models.User{}).Create(&users[i]).Error
		if err != nil {
			log.Fatalf("cannot seed users table: %v", err)
		}
		posts[i].AuthorID = users[i].ID

		err = db.Debug().Model(&models.Post{}).Create(&posts[i]).Error
		if err != nil {
			log.Fatalf("cannot seed posts table: %v", err)
		}
	}

	/* user1 is already an admin
	for i, _ := range userEuiAuths {
		err = db.Debug().Model(&models.UserEuiAuth{}).Create(&userEuiAuths[i]).Error
		if err != nil {
			log.Fatalf("cannot seed userEuiAuths table: %v", err)
		}
	}
	*/
}
