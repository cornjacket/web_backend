module bitbucket.org/cornjacket/web_backend

go 1.13

require (
	bitbucket.org/cornjacket/iot v0.1.6
	bitbucket.org/cornjacket/iot_app/view_client v0.1.0
	bitbucket.org/cornjacket/view_hndlr/app v0.1.2
	bitbucket.org/cornjacket/view_hndlr/client v0.1.2
	github.com/aws/aws-sdk-go v1.33.6
	github.com/badoux/checkmail v0.0.0-20200623144435-f9f80cb795fa
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/gorilla/mux v1.7.4
	github.com/jinzhu/gorm v1.9.14
	github.com/joho/godotenv v1.3.0
	github.com/matcornic/hermes/v2 v2.1.0
	github.com/minio/minio-go/v6 v6.0.57
	github.com/sendgrid/rest v2.6.0+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.6.0+incompatible
	github.com/twinj/uuid v1.0.0
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f
	gopkg.in/go-playground/assert.v1 v1.2.1
)
